---
title: Garuda Apps
description: Learn more about various software that comes with Garuda
published: true
date: 2024-09-13T00:14:43.667Z
tags: garuda-boot, background, splash, garuda network assistant, garuda gamer, garuda settings, kernel, networking
editor: markdown
dateCreated: 2024-09-04T16:17:19.482Z
---

# Other Garuda Applications

## Garuda Boot Options
Garuda Boot Options is a boot options utility. You can tweak several settings related to grub or kernel presets with this handy utility.
![garuda-boot-options.png](/screenshots/garuda-boot-options.png)
This application however requires you to be root, meaning you should be careful while using it to avoid huge mistakes.

> Lets take a look at what the app provides you with.
### Options
In the options section you have various settings that can help in your day to day operations. A **menu timeout**, to choose how long in seconds before the menu loads.
A **boot to**, where you can choose your preferred operating system that would be the default jump to after boot.
You can also toggle to save the last boot choice, which is not recommended, and even toggle to have flat menus.
**Kernel parameters**, allow you to choose how many kernel verbos to be displayed.

### Kernal Parameter presets
This section primarily focuses on the kernel, with some useful functions probably not intended for regular users.
For instance, you can disable **CPU vulnerability mitigations** if you probably want to pentest or maybe do something along the lines of security. You could enable SysRQ to provide you with functions for system debugging. Enabling cgroups v2 on the other hand will provide a mechanism to organize processes hierarchically and distribute system resources along the hierarchy in a controlled and configurable manner.

### Background
While the above functions might be a bit hard to understand, setting background is quite simple. In other words you can change the garuda grub theme and background image in this section.
### Splash
This background is also a bit obvious. You enable a splash screen. 

**N.b:** It's probably a good idea not to change the background/theme and splash. The splash might not even work. 

### Messages

The selected one is probably for the best, but you can mess around to see more verbose or none. This settings will be applied to your grub.

### Log
Finally, you can see all the logs you want
                                                                                                                       
## Garuda Network Assistant                             ![garuda-network-assitant.png](/screenshots/garuda-network-assitant.png)                                                               
This is a network utility with several features that are bound to make your life a little easier.                      
                                                                                    
                                                                                
### Status Tab                                                                                                       
#### Networking                                                                                                 
In this section, you will find the airplane mode option that disables any network traffic. You can       
create a WIFI hotspot and share your network with multiple devices. You also can block or allow        
applications to access your location                                                                                   
#### WiFi Status                                                                                                       
There are two options here that involve either enabling or disabling Bluetooth or your wifi network.                   
#### IP address                                                                                                         
This section shows you the status of your connection and the network addresses and configuration details of     
your network system.                                                                                                   
#### Traceroute                                                                                                        
Troubleshooting has never been easier. In this section, you can check if your connection is going through with ping,    
or how many steps it takes to get there with a traceroute.

### Linux drivers
In this tab, you can list your hardware drivers in the hardware detected section by scanning/rescanning them. You can also view associated Linux drivers below this section and have the ability to scan for them.

 ## Garuda System Maintainance Settings
 ![garuda-system-maintanance-settings.png](/screenshots/garuda-system-maintanance-settings.png)
 This application is a utility that allows you to automatically update core keyrings, or prompts you for critical hotfixes if they are available. You will also receive relevant notifications if required.
>  By default everything is checked, but can be toggled off by the user.
{.is-info}

## Garuda Gamer
![garuda-gamer.png](/screenshots/garuda-gamer.png)
Whether you need to download Games, Emulators or Launchers, Garuda Gamer is the perfect utility for you. It lists and allows you to choose packages that will make gaming smooth from you.
You can also get streaming software, drivers for keyboards, mice and other gaming equipment you may need. 
> For more info related to gaming on garuda, refer to [Gaming on Garuda](/en/gaming-on-garuda).
{.is-info}
## Garuda Settings
![garuda-settings.png](/screenshots/garuda-settings.png)
Garuda Settings is a settings utility that enables you to configure some essential hardware or system parameters.
In the hardware tab, you will see configurations relating to the Hardware Kernel and Language Packages.

#### Hardware Configuration
In this section, a user can auto-install open-source drivers You may notice Display controllers available, whether they are open source, and whether you can install them.
#### Kernel
In the kernel section, a user can see the currently running kernel and all installed kernels in the system.
#### Language Packages
In the language packages section, you can see global language packages and installed language packages. You will also be able to install more language packages here.Refer to 
> [Post Installation tasks/Downloading language packs](/en/post-installation-tasks)
{.is-info}


### System
This section has some configurations you will probably never need once such as:
#### User Accounts
If you want to create users and have a multi-user setup, this section will help you. You will also be able to define whether a user has administrative privileges or not.
#### Time and Date
This is a simple and easier way to set your clock if you don't know how to use either timedatectl or hwclock.
#### Locale Settings
While the locale can easily be set from bars, you can also set it here if you don't find the options to do so in the provided bars.
#### Keyboard settings
An option you will most likely also find in the bar. You can choose the language of your keyboard and reduce errors that arise from using keyboard settings you are not familiar with.