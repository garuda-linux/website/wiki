---
title: Multitouch Trackpad Touchpad Gestures
description: [ 3 | 4 ]-finger-swipe-[ up-down-left-right / pinch-in-out ] Using Fusuma https://github.com/iberianpig/fusuma
published: true
date: 2022-04-22T20:02:18.350Z
tags: guide, system, tutorial, howto, configuration, touchpad, customization, trackpad, gestures, customizations, multitouch, cannabis
editor: markdown
dateCreated: 2022-04-17T19:27:36.530Z
---

Tested on Garuda Kde & Garuda Sway.

> Fusuma uses libinput driver on Linux.
Source (https://github.com/iberianpig/fusuma)

> **NOTE:** **Please see appropriate config.yml for your flavor DE/WM (Kde or Sway) on this wiki page**.

> [Original Garuda Forum Post](https://forum.garudalinux.org/t/guide-for-setting-up-multitouch-touchpad-trackpad-gestures-with-fusuma/18515)
This Wiki Page is the most Up-To-Date.
---
My Touchpad : Apple Magic Trackpad 2

> NOTE: This should work with *any capable trackpad/touchpad*.
---
Sysinfo:
```
OS: Garuda Linux
Host: Apple Inc. Mac-B809C3757DA9BB8D
Kernel: 5.17.1-zen1-1-zen
DE: Plasma 5.24.4
WM: KWin
WM Theme: Sweet-Dark
Terminal: konsole
CPU: Intel i7-6700K (8) @ 4.2GHz
GPU: AMD ATI Radeon R9 M395/ M395X Mac Edition
Memory: 3.72GiB / 23.38GiB
```
---
Let's Go.

* **1. Install fusuma**

```
paru -S ruby-fusuma
```

* **2. Install the following fusuma plugins**

```
paru -S ruby-fusuma-plugin-tap ruby-fusuma-plugin-keypress ruby-fusuma-plugin-wmctrl
```
**NOTE: Do not install the plugin packages *"ruby-fusuma-plugin-sendkey"* or *"ruby-fusuma-plugin-appmatcher"*** --- If you have them installed, remove them. They can cause errors/conflicts with the following config.yml examples provided in this post.

* **3. Create the directory *~/.config/fusuma***

```
mkdir ~/.config/fusuma
```

* **4. Create a new file *~/.config/fusuma/config.yml*** 

* **5. Edit your newly created config.yml to contain the following:**

Use a text editor and place the following content (directly below) into the file and save. This particular config works in Garuda KDE Dr460nized. If you feel Dra460ny, you can refer to the example configs that have been contributed for your convenience [HERE](https://github.com/iberianpig/fusuma/wiki) and adjust your config.yml to your hearts desire.

---
> IIRC this config.yml works on Gnome & XFCE as well. ***Testing needed.***
{.is-info}
---

[ Original Source Used for config.yml Example ] : [KDE to mimic MacOS](https://github.com/iberianpig/fusuma/wiki/KDE-to-mimic-MacOS)
* Note: Edits have been made to the original [ Source ] (below)

> ***Copy/Paste/Save This Section Into ~/.config/fusuma/config.yml***

* **Config For Garuda Kde Edition**

#### ~/.config/fusuma/config.yml for Multitouch Trackpad Gestures in Garuda Kde using fusuma
```
swipe:
  4:
    right:
      command: 'xdotool key alt+Right' # History forward
    left:
      command: 'xdotool key alt+Left' # History back
    up:
      command: 'xdotool key ctrl+t' # Open new tab
      keypress:
        LEFTSHIFT:
          command: 'xdotool key --clearmodifiers ctrl+shift+t' # Open last closed tab
    down:
      command: 'xdotool key ctrl+w' # Close tab
  3:
    left:
      workspace: 'next' # Switch to next workspace
      keypress:
        LEFTSHIFT: 
          window: 'next' # Move window to next workspace
        LEFTMETA:
          command: 'xdotool key --clearmodifiers super+ctrl+Left' # Move window to left side
    right:
      workspace: 'prev' # Switch to previous workspace
      keypress:
        LEFTSHIFT: 
          window: 'prev' # Move window to previous workspace
        LEFTMETA:
          command: 'xdotool key --clearmodifiers super+ctrl+Right'  # Move window to right side
    up:
      command: 'xdotool key Control_L+F10' # Workspace overview
      keypress:
        LEFTMETA:
          window:
            maximized: 'toggle' # Toggle Maximize/Unmaximize Window
    down:
      command: 'xdotool key Control_L+F12' #minimise all windows
      keypress:
        LEFTMETA:
          window: 'close' # Close window

pinch:
  2:
    in:
      command: 'xdotool keydown ctrl click 4 keyup ctrl' # Zoom in
    out:
      command: 'xdotool keydown ctrl click 5 keyup ctrl' # Zoom out
  4:
    in:
      command: 'xdotool key super+a' # Window overview
    out:
      command: 'xdotool key super+s' # Workspace overview

plugin: 
  inputs:
    libinput_command_input: # options for lib/plugin/inputs/libinput_command_input
      enable-tap: true # tap to click
      enable-dwt: true # disable tap while typing
      show-keycodes: true # https://github.com/iberianpig/fusuma-plugin-keypress#add-show-keycode-option
```
[More config.yml Examples](https://github.com/iberianpig/fusuma#more-example-of-configyml)

* **6. Start fusuma in terminal by simply typing the command fusuma**

```
fusuma
```
I suggest first to start fusuma in a terminal, not-daemonized, for initial testing. Simply type `fusuma` in your terminal as instructed above in step 6. fusuma should now be running in your terminal.

* **7. Test**

Try some gestures on your trackpad / touchpad...

You can plainly see which gestures are defined in the ~/.config/fusuma/config.yml that you just created.

Try 3-finger-swipe up/down/left/right, for example.

> You can watch the terminal output from fusuma (in the terminal you started fusuma in) as you are swiping/pinching. This is handy for troubleshooting.
{.is-info}

> **You should now have Multitouch Gestures.**
{.is-success}

* **8. Setup fusuma daemonized so it will automatically run when you login**

Remember, we are running fusuma in a terminal for testing purposes right now as instructed in this guide. You can `ctrl-c` out of that to kill it. Now we want to run fusuma in the background each time we login.

* **8.a: Open KDE Settings/Startup and Shutdown/Autostart**
* **8.b: Click `Add` then `Add Application`**
* **8.c: Type `fusuma -d` in the popup**
```
fusuma -d
```
* **8.d: Click OK**
> DONE. Now fusuma will run daemonized each time you login to KDE.
{.is-success}


You can now logout/login (after killing your current fusuma instance) to make sure it runs when you login.

* **Config For Garuda Sway Edition**

#### ~/.config/fusuma/config.yml for Multitouch Trackpad Gestures in Garuda Sway using fusuma (you can use this config for i3 by replacing each instance of the word `sway` with `i3`)

```
# Custom ~/.config/fusuma/config.yml for Garuda Sway Edition
#
# config.yml for fusuma --- https://github.com/iberianpig/fusuma
#
# This should work in any linux distro running sway and using fusuma.
#
# Author: Donald Tanner (Cannabis on Garuda Forum)
# Email:  
#
# Note, this is not a file included in Garuda Official.
# This is user contributed to help others along the way.
#
# swipe section
swipe:
  3:
    left:
      command: exec sway move left # Move Window Left
    right:
      command: exec sway move right #  Move Window Right
    up:
      command: exec sway move up # Move Window Up
    down:
      command: exec sway move down # Move Window Down
  4:
    left:
     command: exec sway workspace next # Next Workspace
    right:
      command: exec sway workspace prev # Previous Workspace
    up:
      command: exec sway fullscreen toggle # Fullscreen Toggle
    down:
      command: exec sway floating toggle # Tiling Floating Toggle
# pinch section
pinch:
  2:
    in:
      command: 'xdotool keydown ctrl click 4 keyup ctrl' # Zoom In - Test in a browser
    out:
      command: 'xdotool keydown ctrl click 5 keyup ctrl' # Zoom Out - Test in a browser
  4:
    in:
      command: 'xdotool key super+a' # Window Overview - Mod+a not defined in sway
                                     # Redefine to something useful
    out:
      command: 'xdotool key super+s' # Workspace Overview - Mod+s not defined in sway
                                     # Redefine to something useful
threshold:
  swipe: 0.4
  pinch: 0.4

interval:
  swipe: 0.8
  pinch: 0.1
```
---
Note: Use Sway/i3 autostart procedure to run the `fusuma -d` daemon in the background with each login.

Conculsion: I find fusuma to be very reliable as well as cross-desktop.

[My short video on Odysee showing ~/.config/fusuma/config.yml and 3-finger swiping in KDE](https://odysee.com/@Linux:c/Multitouch-Touchpad-Trackpad-Gestures-On-Linux-With-fusuma-On-Garuda-Linux:8)

[Another (more complete video) setting up fusuma by another user on Odysee](https://odysee.com/multitouch-touchpad-gestures-in-linux:944fafd9ed1abf3be3ccc3f536149910d1a96fce)

