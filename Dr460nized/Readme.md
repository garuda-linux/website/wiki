---
title: Dr460nized Readme
description: Readme about Dr460nized Theme
published: true
date: 2025-01-28T01:18:19.121Z
tags: dr460nized, readme
editor: markdown
dateCreated: 2025-01-14T23:05:08.937Z
---

## Readme about Dr460nized Theme

This wiki article provides insight into various specificities or questions about the Dr460nized Theme. 



### ForceBlur Effect (also called BetterBlur)

Dr460nized installs as a dependency the package kwin-effects-forceblur. This provides blur effects on windows and menus. However according to the [source](https://github.com/taj-ny/kwin-effects-forceblur), it could be resource-intensive, especially if you have many windows open.

On Wayland, high GPU load may result in higher cursor latency or even stuttering. If that bothers you, set the following environment variable:
**KWIN_DRM_NO_AMS=1**.

If that's not enough, try enabling or disabling the software cursor by also setting:
**KWIN_FORCE_SW_CURSOR=0**
or **KWIN_FORCE_SW_CURSOR=1**.

Intel GPUs use software cursor by default due to this bug, however it doesn't seem to affect all GPUs.