---
title: Dr460nized Plasma 6 migration (Deprecated 2025-01-01)
description: How to migrate to the new Plasma 6 layouts
published: true
date: 2025-03-05T00:17:07.505Z
tags: dr460nized, plasma 6, migration, garuda-system-maintenance
editor: markdown
dateCreated: 2024-02-29T10:06:55.447Z
---

### Dr460nized KDE 6 migration

This wiki article provides insight into the currently required manual intervention, caused by the [KDE Plasma 6 megarelease](https://kde.org/announcements/megarelease/6). 

Manual intervention is required because starting with KDE 6, Plasmoids, Global Themes, and SDDM Themes need to be ported to the new desktop environment. There will be a few changes regarding the Dr460nized desktop to embrace those changes. While the overall desktop aesthetics stay roughly the same, a few features will return at a later date once they have been implemented upstream.

Garuda System Maintenance will help apply the needed migration, providing a popup window to complete the action once the necessary conditions exist.

**However, if you are not using Dr460nized Global Theme and Dr460nized Plasma Style, you are on your own to port your custom theme or rely on the Theme's creator to do it.**


### What are the major differences?

- Of course, all the latest and greatest changes KDE 6 brings to us, like:
	- Wayland as the default session
  - System tray icons are sourced from the current System Icons theme instead of shipping custom ones or defaulting to Breeze's.
  - Partial HDR support
  - A new sound theme called "Ocean"
  - Floating Panels and improved configuration
  - New Overview effects and some others
  - The Desktop Cube is brought back!
  - Almost flawless Touchpad gestures
  - Much less buggy environment
- No blur behind application windows for now - likely returns as soon as [Kvantum is compiled against KDE 6 lib](https://github.com/tsujan/Kvantum/blob/master/Kvantum/INSTALL.md#with-cmake). We ship a custom Sweet theme which has translucent effects disabled until this is in Arch repos.
- Can't double click app menu bar to toggle maximize - we no longer use the Window App Menu plasmoid but KDE's native Global app menu. This option isn't present here, as many other configurations of Window App Menu had.
- The default SDDM Theme is now based on Breeze. We simply called it "Dr460nized". The previous Dr460nized-Sugar-Candy is still available and seems to work, but it has not been ported and may misbehave with time.

We tried to minimize impacts on theming. Here's what the Dr460nized Theme looks like in Plasma 6 (1920x1080 resolution). Pretty much the same as before :) 

![dr460nized_p6.png](/dr460nized_p6.png)

The result may vary depending on the monitor resolution you are running at. For example, the bigger your resolution, the smaller the Digital Clock's fonts will be and the bigger the bottom Dock will be. Of course, both are easily adjustable, there is no need to freak out if you see that. :)

### How to perform migration?

This is one of the times when Garuda System Maintenance shines. Of course, it can be done manually as well. Important: the main condition for triggering any of this is having a fully up-to-date system and an active Dr460nized theme - `garuda-update` will, as usual, help with this task. It specifically auto-answers the needed prompts for package removals you will encounter during the KDE 6 update.

** It is extremely highly recommended to reboot immediately after performing the upgrade! It is possible your usual GUI Logout, Restart, and Shutdown buttons won't work. Don't panic, all you have to do is open up a Terminal window (which may well still be opened after your garuda-update) and type in
~~~
systemctl reboot
~~~
That's it! On the next reboot, you will be greeted with our current Dr460nized Plasma 6 SDDM Theme.

#### Via Garuda System Maintenance

There are two ways of applying the migration **after** your initial Plasma 6 boot:

- Accepting the automatic migration prompt: The application will try to detect existing KDE 6 setups with `garuda-dr460nized=>4.0.0-1` packages, offering the migration to the new layout. Here, you just need to confirm the prompt. This might not be applicable depending on the changes made to out-of-the-box theming. When applicable, it will reset the theme configuration to the current Dr460nized Plasma 6 theme. It is always a good practice to at minimum log out and re-log in to the desktop.
  ![dr460nized_p6_prompt.png](/screenshots/dr460nized_p6_prompt.png)
- Triggering the migration manually: head over to the system tray, find the System Maintenance icon, and right-click it. Then select “Dr460nized migration” to execute the needed action. Yet again, this will reset the theme configuration to the current Dr460nized Plasma 6 theme. It is always a good practice to at minimum log out and re-log in to the desktop.

  ![dr460nized_p6_tray.png](/screenshots/dr460nized_p6_tray.png)

#### Manually

This can be done by opening Konsole (or another terminal emulator of choice) and running the following:

~~~
lookandfeeltool --resetLayout --apply Dr460nized
~~~

This will apply the "Dr460nized" global layout, resetting the layout back to the updated one shipped in `garuda-dr460nized=>4.0.0-1`, **even if you run a fully custom theme**. It is always a good practice to at minimum log out and re-log in to the desktop.

### Some known issues

- Kvantum does not seem to read the Global Theme configuration. Since the Plasma Blur effect is no longer working at this time and Kvantum does not apply our temporary style, you have to open Kvantum Manager and Change the theme for `Dr460nized`. This will remove transparency in some windows, like Dolphin, which will make it much easier to view.