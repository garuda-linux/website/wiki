---
title: Multiple installations on one partition
description: A quick guide to setting up multiple installations on one Btrfs partition
published: true
date: 2023-03-18T15:56:47.299Z
tags: 
editor: markdown
dateCreated: 2023-03-18T15:56:47.299Z
---

# Multiple installations on one partition

It is possible to have multiple Garuda (or other Linux) installations on a single partition using Btrfs subvolumes. The basic steps for configuring this kind of setup are outlined here.

> Like all multibooting setups, this should be considered *not* offically supported by the Garuda team. Multibooting adds extra layers of complexity to a system that can make troubleshooting more difficult. Be warned that manipulating subvolumes and bootloader configurations like described in this topic can make your system unbootable if you make a mistake. 
{.is-warning}


## Rename the default subvolumes
Create a mount point outside the top-level subvolumes. In my example here, my Btrfs partition is on `nvme0n1p2`. Obviously change that to whatever your Btrfs partition happens to be.
~~~
sudo mkdir /mnt/top-level_subvolume
sudo mount -o subvolid=0 /dev/nvme0n1p2 /mnt/top-level_subvolume
cd /mnt/top-level_subvolume
~~~
Rename all the subvolumes. Replace `gnome` , `gnome_cache`, etc from the example here with whatever names you wish to use for your subvolumes.
~~~
sudo mv @ gnome
sudo mv @cache gnome_cache
sudo mv @home gnome_home
sudo mv @log gnome_log
sudo mv @root gnome_root
sudo mv @srv gnome_srv
sudo mv @tmp gnome_tmp
~~~
It is customary to mark these subvolumes with "@". This convention can be preserved or not depending on your preference. In this example, "@" has not been used in the subvolume names.

## Update `/etc/fstab`

It is important to get your `/etc/fstab` updated **before you reboot.** Take extra care to ensure the entries are accurate.

Update the `subvol=` values to the new names (i.e. `subvol=@` should be changed to `subvol=gnome`, and so on.)

Here is an example, continuing with the "`gnome`" subvolume names:

~~~
<device>																	<mount point>  <type>  <options>
UUID=xxxx-xxxx                            /boot/efi      vfat    defaults,noatime 0 2
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /              btrfs   subvol=/gnome,noatime,compress=zstd 0 0
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /home          btrfs   subvol=/gnome_home,noatime,compress=zstd 0 0
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /root          btrfs   subvol=/gnome_root,noatime,compress=zstd 0 0
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /srv           btrfs   subvol=/gnome_srv,noatime,compress=zstd 0 0
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /var/cache     btrfs   subvol=/gnome_cache,noatime,compress=zstd 0 0
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /var/log       btrfs   subvol=/gnome_log,noatime,compress=zstd 0 0
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /var/tmp       btrfs   subvol=/gnome_tmp,noatime,compress=zstd 0 0
tmpfs                                     /tmp           tmpfs   defaults,noatime,mode=1777 0 0
~~~

## Update Grub

Change the name of the boot directory from "Garuda" to something else so it won't create a conflict when you install the next ISO. The next installation will also name the boot directory "Garuda", which will simply overwrite any directory with the same name (this would make our first installation unbootable).
~~~
sudo mv /boot/efi/EFI/Garuda /boot/efi/EFI/Gnome
~~~

Change the `GRUB_DISTRIBUTOR=` line in `/etc/default/grub` to match whatever you just named the boot directory in `/boot/efi/EFI/`. 
~~~
sudo micro /etc/default/grub
~~~
~~~
GRUB_DISTRIBUTOR="Gnome"
~~~
If this line does not match the name of a directory in `/boot/efi/EFI` (the one we just changed above), when you run the `grub-install` script it will automatically make a completely new directory.

Next, run the Grub installation script.
~~~
sudo grub-install ...
~~~
I have put "`...`" to mean "add whatever options are specifically relevant for your Grub installation". In some cases, `grub-install`  or `grub-install --no-nvram` is enough. If you are not sure, refer to the document here: https://wiki.archlinux.org/title/GRUB

Regenerate the Grub configuration file.
~~~
sudo update-grub
~~~

## Add another installation
Boot to the live environment and start the installer. Choose the manual partitioning option.

Select the EFI partition and click on <kbd>Edit</kbd>. Be very careful with the selections you make--you want to *keep* the contents of the partitions--***do not format*** or you will lose the first installation.

>   ### EFI partition:
>   * **Content: Keep**
>   * **Mount point: `/boot/efi`**

Next, select the Btrfs partition and click on <kbd>Edit</kbd>.

>### Btrfs partition:
>* **Content: Keep**
>* **Mount point: `/`**


Proceed with the rest of the installation as you normally would.

This process can be repeated as many times as needed, so long as you continue to choose unique names for the subvolumes and Grub distributor entries each time.