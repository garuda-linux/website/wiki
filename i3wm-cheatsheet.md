---
title: i3wm-cheatsheet
description: i3 shortcuts
published: true
date: 2024-04-25T12:59:44.152Z
tags: 
editor: markdown
dateCreated: 2024-04-23T22:20:15.354Z
---

> **Note:** the default <kbd>Mod</kbd> key is the Meta/Super key.
> * On a Windows keyboard, this will be the "Windows" key.
> * On an Apple keyboard, this will be the "Command" key.
>
> If you wish, you can change the <kbd>Mod</kbd> key to something else in your i3 config.
{.is-info}

## Application Shortcuts

|Description                             |Keybinds                 |
|---|---|
|Launch preferred Browser (firedragon)   |Mod + F1                 |
|Launch preferred MaileClient (geary)    |Mod + F2                 |
|Launch preferred FileManager (thunar)   |Mod + F3                 |
|Launch preferred Text Editor (geany)    |Mod + F4                 |

## ScreenCapture And Print Functionality


|Description                        |Keybinds                 |
|---|---|
|Launch Screen Capture (flameshot)  |Mod + F9                 |
|Print Screen (flameshot)           |Print                    |

## Terminal Emulators & Progam Launchers

|Description              |Keybinds                 |
|---|---|
|Launch Terminal (Kitty)  |Mod + Return             |
|Launch Apps (dmenu)      |Mod + SHIFT + d          |
|Launch Cheatsheet        |Mod + SHIFT + i          |
|Launch Rofi              |Mod + d                  |
|Launch Terminal (kitty)  |Mod + t                  |

## System & Flexibility Commands

|Description                             |Keybind                  |
|---|---|
|hide/unhide i3 bar                      |Mod + m                  |
|Lock screen (i3lock)                    |Mod + l                  |
|Garuda Linux install (calamares)        |Mod + i                  |
|Reload i3 configuration                 |Mod + SHIFT + c          |
|Restart i3 inplace                      |Mod + SHIFT + r          |
|Logout, Reboot & Shutdown               |Mod + 0                  |

> Brightness Controls/ Volume Controls and Music Controls do exist. By pressing the FN key + respective icons on the keyboard will work, since they have already been configured in the i3 configuration.

## Terminating Windows

|Description              |Keybind                  |
|---|---|
|Kill focused window      |Mod + c                  |
|Kill focused window      |Mod + q                  |

## Change Focus

|Description              |Keybind                  |
|---|---|
|Focus left               |Mod + Left               |
|Focus down               |Mod + Down               |
|Focus up                 |Mod + Up                 |
|Focus right              |Mod + Right              |

## Move Focused Window

|Description                        |Keybind                  |
|---|---|
|Move Focused Window Left           |Mod + SHIFT + Left       |
|Move Focused Window Down           |Mod + SHIFT + Down       |
|Move Focused Window Up             |Mod + SHIFT + Up         |
|Move Focused Window Right          |Mod + SHIFT + Right      |

## Split Orientation

|Descritpion              |Keybind                  |
|---|---|
|Split horizontaly        |Mod + h                  |
|Split Verticaly          |Mod + v                  |


## Productivity Commands

|Description                             |Keybinds                 |
|---|---|
|Toggle floating mode                    |Mod + space              |
|Toggle Tiling/floating Windows          |Mod + SHIFT + space      |
|Toggle sticky                           |Mod + SHIFT + s          |
|Focus parent container                  |Mod + a                  |
|Move Container to Scratchpad            |Mod + SHIFT + minus      |
|Show / hide Scratchpad                  |Mod + minus              |


## Workspace Navigation and Operations

## Navigating Workspaces

|Description                             |Keybinds                 |
|---|---|
|Move to next workspace                  |Mod + Ctrl + Right       |
|Move to previous workspace              |Mod + Ctrl + Left        |
|Move to next workspace                  |Alt + Ctrl + Right       |
|Move to previous workspace              |Alt + Ctrl + Left        |

## Switch Workspace

|Description              |Keybinds                 |
|---|---|
|Switch to workspace 1    |Mod + 1                  |
|Switch to workspace 2    |Mod + 2                  |
|Switch to workspace 3    |Mod + 3                  |
|Switch to workspace 4    |Mod + 4                  |
|Switch to workspace 5    |Mod + 5                  |
|Switch to workspace 6    |Mod + 6                  |
|Switch to workspace 7    |Mod + 7                  |
|Switch to workspace 8    |Mod + 8                  |

## Move Focused Container To Workspace

|Description                                  |Keybinds             |
|---|---|
|Move Container to workspace 1                |Mod + SHIFT + 1      |
|Move Container to workspace 2                |Mod + SHIFT + 2      |
|Move Container to workspace 3                |Mod + SHIFT + 3      |
|Move Container to workspace 4                |Mod + SHIFT + 4      |
|Move Container to workspace 5                |Mod + SHIFT + 5      |
|Move Container to workspace 6                |Mod + SHIFT + 6      |
|Move Container to workspace 7                |Mod + SHIFT + 7      |
|Move Container to workspace 8                |Mod + SHIFT + 8      |

## Resize Windows
> You can also use the mouse


|Description                   |Keybinds                      |
|---|---|
|Shrink 5px to the left        |Mod + left                    |
|Grow 5px down                 |Mod + down                    |
|Shrink 5px  Up                |Mod + up                      |
|Grow 5px to the right         |Mod + right                   |
|Mode default                  |Mod + return                  |
