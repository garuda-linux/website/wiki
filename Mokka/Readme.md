---
title: Readme
description: Readme about Dr460nized Theme
published: true
date: 2025-03-02T13:35:34.449Z
tags: 
editor: markdown
dateCreated: 2025-01-28T01:22:45.362Z
---

## Readme about Mokka Theme

This wiki article provides insight into various specificities or questions about the Mokka Theme, not referenced in the [Release Article](https://wiki.garudalinux.org/e/en/Mokka/Mokka-migration).



### ForceBlur Effect (also called BetterBlur)

Mokka installs as a dependency the package `kwin-effects-forceblur`. This is different than the default **Blur** effect included in Plasma. It provides of course blur effects on windows, docks and menus, but also allows control over which of the 3 blurs and uses an include/exclude list of **Window Classes**. For example, with this feature we are able to not blur docks and exclude window class *xwaylandvideobridge* apps, which should not blur.
However according to the [source](https://github.com/taj-ny/kwin-effects-forceblur), it could be resource-intensive, especially if you have many windows open.

On Wayland, high GPU load may result in higher cursor latency or even stuttering. If that bothers you, set the following environment variable:
**KWIN_DRM_NO_AMS=1**.

If that's not enough, try enabling or disabling the software cursor by also setting:
**KWIN_FORCE_SW_CURSOR=0**
or **KWIN_FORCE_SW_CURSOR=1**.

Intel GPUs use software cursor by default due to this bug, however it doesn't seem to affect all GPUs.


---

#### NEW PANEL-COLORIZER FEATURE
The new [Panel-Colorizer](https://github.com/luisbocanegra/plasma-panel-colorizer) applet allows including full transparency to both the Top Panel and Bottom Dock.
It can do a LOT more, you can customize many UI elements of your Plasma panels/docks.
If you want to use a different preset, do the following:
- Right-click the Kickoff menu logo or System Tray chevron and select
~~~
Configure Panel-Colorizer...
~~~
- In the Presets section you can select something different.
-Attention needed here, a preset can load another preset based on certain criteria. You can find this in the **Presets auto-loading** section.
- Very important to take the time to go through all the possible settings and sections, it is the only way to understand what (and how) that amazing applet can do.


