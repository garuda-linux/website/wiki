---
title: Release Notes and Migration
description: How to migrate to the new Plasma 6 layouts
published: true
date: 2025-03-07T20:21:57.500Z
tags: mokka
editor: markdown
dateCreated: 2025-01-28T01:31:45.968Z
---

# NEW SPIN (as of 2025-03-07)


Welcome to **Garuda Mokka**!
[Catppuccin](https://catppuccin.com/) flavor deeply integrated into Arch Linux along side Garuda's feature-full packages.

## THE GOAL
Garuda has many spins with striking colors and KDE-based ones are pushing this quite far.
We wanted to offer a KDE spin that would be virtually the exact opposite of **Dr460nized**.
Dr460nized is a positively aggressive and very awake spin, now you have one very muted, relaxing and zen.

## WHICH COLOR PALETTE
With such different color mixes, we opted only for 1 customized **Catppuccin-Mocha-Mauve** flavor out of the box. See the [palette](https://catppuccin.com/palette) for colors used.
The reason for this is there are so many possible combos it would have been too much work to offer (and maintain) them all, would have taken more ISO and installed system space and a lot of Global Themes to choose from, which could be confusing or too much for people.

## WHY
This brings to the point: Why Catppuccin?
Because not only it is Community-based, it has lots of flavors to choose from, is very popular everywhere and in Feb 2025 had 394 [ports!](https://catppuccin.com/ports) Mixing this with Garuda became a must. Therefore, some Garuda [websites](https://gitlab.com/garuda-linux/website/website-catppuccin) and its [Forum](https://forum.garudalinux.org/) also offer Catppuccin colors!

## WHAT
What do we include with this spin?

The basic Garuda tools packages (all non-gamers) you find in Dr460nized are also offered in Mokka. That's the foundation.
Then our famous **Firedragon** browser totally did not fit! This is why we are offering a new skinned version: **firedragon-catppuccin**.
It uses a theme that matches the one we chose for Mokka, even the icon and logos have been re-worked.
It packages, in place or on top of standard Firedragon, the following:

- [Catppuccin Floorp-oriented](https://addons.mozilla.org/en-US/firefox/addon/catppuccin-mocha-mauve-floorp/) theme
- [Styl-us](https://addons.mozilla.org/en-US/firefox/addon/styl-us/) extension, for those wanting to view over 110 popular websites in Catppuccin colors. The extension is ready to receive Catppuccin [userstyles](https://github.com/catppuccin/userstyles).

For **Plasma 6.3.1** we packaged the following themes:
- [KDE](https://github.com/catppuccin/kde) Global Theme
- [GTK](https://github.com/vinceliuice/Colloid-gtk-theme) GTK Theme
- [Micro](https://github.com/catppuccin/micro) terminal editor
- [Conky](https://github.com/catppuccin/conky) manager
- [Fish](https://github.com/catppuccin/fish) shell
- [Konsole](https://github.com/catppuccin/konsole) terminal
- [Bat](https://github.com/catppuccin/bat)
- [GRUB](https://github.com/catppuccin/grub)
- [TTY](https://github.com/catppuccin/tty)
- Google **Inter** fonts for the system and **JetBrains Mono Nerd** for terminal
- [Tela Circle Dracula](https://github.com/vinceliuice/Tela-circle-icon-theme) icons theme
- Custom **Plasma Style** based on [Utterly-Round](https://github.com/HimDek/Utterly-Round-Plasma-Style)
- Slightly customized **SDDM6 Log in screen** based on [Magna](https://github.com/L4ki/Magna-Plasma-Themes) from l4k1
- New **Wallpapers** fitting dark Catppucin palette
- 3 [Panel-Colorizer](https://github.com/luisbocanegra/plasma-panel-colorizer) presets
	2 on the **Top Panel** (more on that below)
	1 on the bottom **Dock**
	Two of them together are bringing back the fully transparent look we used to have with Latte-Dock.
	The third one, for the Top Panel, is when a window touches the panel or is maximized, then a special panel theme (preset) activates (built-in "Mokka Carbon") for 2 reasons:
		- A maximized window with a transparent panel does not look good, it requires more opacity
		- Notify people at least 1 window is touching the panel or is maximized


---

### MIGRATION FROM OTHER GLOBAL THEME
> Please read the procedure **BEFORE** starting the migration
{.is-warning}

If you don't want to install from the ISO but would like to migrate to the full **Garuda Mokka** theme, here are the steps to follow coming from a non-customized Dr460nized installation
> Although this should work from any custom theme, you may need some more steps to change something else not part of Dr460nized installation or encounter some different results. For instance you could be missing packages like Dekstop Effects.

> There is no automated rollback possible.
You would have to manually undo what you did by reinstalling `garuda-dr460nized` and `firedragon`.
The themes conflict with each other because they write into the same files for some of them. As such, it is a good practice to write down what the configs are set at before you overwrite them.
{.is-warning}

- Backup your current installation your preferred way
- Fully update your system. **Skipping this step may have huge consequences**
~~~
garuda-update
~~~
- Install **garuda-mokka** and **firedragon-catppuccin** and accept removal of all suggested packages
~~~
pacman -S garuda-mokka firedragon-catppuccin
~~~
- Do not logout nor reboot/shutdown and keep a Terminal window open
- In **System Settings**
	*Global Theme* - apply Appearance and Desktop settings of Mokka theme.
- Reboot	
  -During the reboot process, you should already have the new GRUB theme, SDDM theme, Plasma settings and Wallpaper.
  **SDDM is showing a big virtual keyboard, a blue background and looks like no theme at all? Don't panic, we'll get into that later.**

However everything else requires manual intervention because they affect files in your **$HOME** folder.

- Open **Kvantum Manager**, `Change` the theme for *Mokka* and `Use This Theme`
- Go back in **System Settings**
	-*Text & Fonts* - change the font for `Inter`, except `Liberation Mono` for fixed width
	-*Window Management* - *Desktop Effects*, make sure `Blur` is disabled and `BetterBlur` is enabled, then open `Rounded Corners` settings, go in Inclusions & Exclusions tab and add `xwaylandvideobridge` to the exclusion list (you may need to hit **Refresh** first). Then go in `Outlines` tab and change the settings for the following (including the Outline transparency that you barely see the super small indicator for!):

![mokka_rounded_corners.png](/mokka_rounded_corners.png)
Normally you should use **Highlight** and **ToolTip Text** or Text for the *Decoration Colors*.
The Custom colors are **#cba6f7** (Mauve) and **#b4befe** (Lavender).
> You may have to force the colors in Rounded Corners outlines, sometimes the effects does not retrieve the proper colors from the system palette, therefore selecting from Decoration Color drop down may give you a different color!
{.is-info}


- For **KDE Syntax Highlighting**, open your text editor (for example Kate) and change the *Editor Color Theme* (in Kate it's called like that) for
~~~
Mokka
~~~
- For **Konsole**, edit `~/.local/share/konsole/Garuda.profile` and replace *ColorScheme* and *Font* lines with
~~~
	ColorScheme=Mokka
	Font=JetBrainsMono Nerd Font,12,-1,5,700,0,0,0,0,0,0,0,0,0,0,1,Bold
~~~
- For **Fish** theme, copy `/etc/skel/.config/fish/themes/Catppuccin\ Mocha.theme` to `~/.config/fish/themes/`. Create the folder if it doesn't exist
	-In terminal, execute
~~~
yes | fish_config theme save "Catppuccin Mocha"
~~~
- For **Starship**, copy `/etc/skel/.config/starship-mokka.toml` to `~/.config/starship.toml`
- For **Fastfetch**, in your config file located in `~/.config/fastfetch/`, edit the *source* line at the beginning where it fetches a PNG and change it for `/usr/share/icons/garuda/mokka-fastfetch.png`, then change the last line in `~/.config/fish/config.fish` where it loads a config for
~~~
fastfetch --load-config mokka.jsonc
~~~
- For `Micro`, copy `/etc/skel/.config/micro/colorschemes/catppuccin-mocha.micro` to `~/.config/micro/colorschemes/`. Create the folder if it doesn't exist
	-Change the *colorscheme* line in `~/.config/micro/settings.json` for
~~~
  catppuccin-mocha
~~~
- For **bat** theme, create folder `~/.config/bat/themes` and drop in the files `/etc/skel/.config/bat/config` in `~/.config/bat/` and `/etc/skel/.config/bat/themes/Catppuccin\ Mocha.tmTheme` to `~/.config/bat/themes/`
	-In terminal, execute
~~~
bat cache --build
~~~
- For **Screen Locker** wallpaper, if it did not apply, edit `~/.config.kscreenlockerrc` and replace the *Greeter* and *Greeter/Wallpaper/General* sections with the following:
~~~
[Greeter]
Theme=Mokka
WallpaperPlugin=org.kde.image

[Greeter][Wallpaper][org.kde.image][General]
Image=/usr/share/sddm/themes/Mokka/background.png
~~~
#### Fixing the no SDDM theme issue
This situation might occur when your SDDM configuration files do not follow the current standard, mainly in the location of the files or in which one you set the theme. Possible if you have an old installation things have moved around with recent Plasma versions.
No matter the reason, edit `/etc/sddm.conf.d/sddm.conf`, go in the *Themes* section, find the lines with the theme name and cursor theme and change them for
~~~
Current=Mokka
CursorTheme=Catppuccin-Mocha-Mauve
~~~


---


#### GTK THEME
It is possible your GTK theme does not display the proper theme (almost 100% certain!).
To use the proper theme, perform the following:
- Verify if you have a theme set in GTK_THEME variable
~~~
env | grep GTK_THEME
~~~
If it returns nothing (normal case):
- Go in *System Settings*
-*Application Styles*, then *Configure GNOME/GTK Application Style* in top right or click the 3 vertical dots followed by Configure GNOME/GTK and change the theme for `Catppuccin-Purple-Dark-Catppuccin`.


If it returns something (unusual case), you have to unset (make sure you won't impact something else on your machine, that is up to you to validate first):
~~~
unset GTK_THEME # in bash shell
set -e GTK_THEME # in fish shell
~~~
- Then go in *System Settings*
-*Application Styles*, then *Configure GNOME/GTK Application Style* in top right or click the 3 vertical dots followed by Configure GNOME/GTK and change the theme for `Catppuccin-Purple-Dark-Catppuccin`.

---
