---
title: Hyprland Cheatsheet
description:
published: true
date: 2024-09-16T15:43:28.806Z
tags: hyprland, hyprland binds
editor: markdown
dateCreated: 2024-03-27T15:42:35.263Z
---

> **Note:** the default <kbd>Mod</kbd> key is the Meta/Super key.
> * On a Windows keyboard, this will be the "Windows" key.
> * On an Apple keyboard, this will be the "Command" key.
>
> If you wish, you can change the <kbd>Mod</kbd> key to something else in your Hyprland config.
{.is-info}

## Application Shortcuts
|Descripton                        |Keybinds                            |
|----------------------------------|------------------------------------|
|Launch Terminal                   |Mod + T/Enter                       |
|Launch File Manager               |Mod + N                             |
|Launch Firedragon                 |Mod + F1                            |
|Launch Thunderbird                |Mod + F2                            |
|Launch Thunar (file manager)      |Mod + F3                            |
|Launch Geany                      |Mod + F4                            |
|Luanch Github-Desktop             |Mod + F5                            |
|Launch Gparted                    |Mod + F6                            |
|Launch Inkscape                   |Mod + F7                            |
|Launch Blender                    |Mod + F8                            |
|Luanch Meld                       |Mod + F9                            |
|Launch Joplin-desktop             |Mod + F10                           |
|Launch Snapper-tools              |Mod + F11                           |
|Launch Galculator                 |Mod + F12                           |

## Launchers
|Description                        |Keybinds                           |
|-----------------------------------|-----------------------------------|
|Launch App Finder #wofi            |Mod                                |
|Launch bar #nwg                    |Mod + SHIFT + e                    |
|Launch App Drawer                  |Mod + SHIFT + d                    |
|Launch Cliphist #wofi              |Mod + v                            |

## Window Flexibility & System Commands
|Description                        |Keybinds                           |
|-----------------------------------|-----------------------------------|
|Toggle pseudo mode #Dwindle        |Mod + p                            |
|Toggle split #Dwindle              |Mod + SHIFT +p                     |
|Toggle floating mode               |Mod + SHIFT +SPACEBAR              |
|Kill active window                 |Mod + q                            |
|Scroll existing workspaces         |Mod + Mouse Scroll                 |
|Reload hyprland configuration      |Mod + SHIFT + r                    |
|Fullscreen                         |Mod + f                            |
|Fullscreenstate                    |Mod + SHIFT + f                    |
|Maximize window                    |Mod + m                            |
|Move windows                       |Mod + Left Mouse Click             |
|Resize windows                     |Mod + Right Mouse Click            |
|Toggle special workspace                  |Mod + SIFT + s                     |

## Focus On Windows In Workspaces
|Description              |Keybinds                 |
|-------------------------|-------------------------|
|Focus on left window     |Mod + left arrow key     |
|Focus on left window     |Mod + h                  |
|Focus on right window    |Mod + right arrow key    |
|Focus on right window    |Mod + l                  |
|Focus on upper window    |Mod + Up arrow key       |
|Focus on upper window    |Mod + k                  |
|Focus on window below    |Mod + down arrow key     |
|Focus on window below    |Mod + j                  |

## Move Windows In Workspaces
|Description             |Keybinds                          |
|------------------------|----------------------------------|
|Move to left window     |Mod + SHIFT + left arrow key      |
|Move to left window     |Mod + SHIFT + h                   |
|Move to right window    |Mod + SHIFT + right arrow key     |
|Move to right window    |Mod + SHIFT + l                   |
|Move to upper window    |Mod + SHIFT + Up arrow key        |
|Move to upper window    |Mod + SHIFT + k                   |
|Move to window below    |Mod + SHIFT + down arrow key      |
|Move to window below    |Mod + SHIFT + j                   |

## Switch Workspaces
|Description                       |Keybinds                            |
|----------------------------------|------------------------------------|
|Move to workspace 1               |Mod + 1                             |
|Move to workspace 2               |Mod + 2                             |
|Move to workspace 3               |Mod + 3                             |
|Move to workspace 4               |Mod + 4                             |
|Move to workspace 5               |Mod + 5                             |
|Move to workspace 6               |Mod + 6                             |
|Move to workspace 7               |Mod + 7                             |
|Move to workspace 8               |Mod + 8                             |
|Move to workspace 9               |Mod + 9                             |
|Move to workspace 10              |Mod + 0                             |

## Move To Workspace With Focused Container
|Description                          |Keybinds                      |
|-------------------------------------|------------------------------|
|Move with window to workspace 1      |ALT + SHIFT + 1               |
|Move with window to workspace 2      |ALT + SHIFT + 2               |
|Move with window to workspace 3      |ALT + SHIFT + 3               |
|Move with window to workspace 4      |ALT + SHIFT + 4               |
|Move with window to workspace 5      |ALT + SHIFT + 5               |
|Move with window to workspace 6      |ALT + SHIFT + 6               |
|Move with window to workspace 7      |ALT + SHIFT + 7               |
|Move with window to workspace 8      |ALT + SHIFT + 8               |
|Move with window to workspace 9      |ALT + SHIFT + 9               |
|Move with window to workspace 10     |ALT + SHIFT + 0               |

## Move An Active Window To A Workspace Silently
|Description                        |Keybinds                           |
|-----------------------------------|-----------------------------------|
|Move window only to workspace 1    |Mod + SHIFT + 1                    |
|Move window only to workspace 2    |Mod + SHIFT + 2                    |
|Move window only to workspace 3    |Mod + SHIFT + 3                    |
|Move window only to workspace 4    |Mod + SHIFT + 4                    |
|Move window only to workspace 5    |Mod + SHIFT + 5                    |
|Move window only to workspace 6    |Mod + SHIFT + 6                    |
|Move window only to workspace 7    |Mod + SHIFT + 7                    |
|Move window only to workspace 8    |Mod + SHIFT + 8                    |
|Move window only to workspace 9    |Mod + SHFIT + 9                    |
|Move window only to workspace 10   |Mod + SHIFT + 0                    |

## Impelement optional features
|Description                        |Keybinds                           |
|-----------------------------------|-----------------------------------|
|Enable/Disbale G-Hyprland Script   |Mod + SHIFT + G                    |
|Launch G-Hyprland (after enable)   |Mod + S                            |
|Show/Hide nwg-dock                 |ALT + SHIFT + H                    |

