---
title: Garuda Fish CheatSheet
description: 
published: true
date: 2024-09-15T18:49:40.707Z
tags: fish shell, fish, aliases
editor: markdown
dateCreated: 2024-09-15T17:02:44.058Z
---

## Garuda Fish Aliases                                   
Here are some common Garuda aliases that will make your fish shell experience a little smoother  

### New to Arch From Debian
| Command                                                                 | Alias/Shortcut | Purpose                                                    |  
|-------------------------------------------------------------------------|----------------|------------------------------------------------------------|                                                                                    
| `man pacman`                                                            | `apt`          | Now you can learn about pacman                             |                                        
| `man pacman`                                                            | `apt-get`      | Now you can leaern about pacman                            |                                             
| `sudo`                                                                  | `please`       | Please give me root                                        |                                             
| `nc termbin.com 9999`                                                   | `tb`           | A command line pastebin for you                            |                                             
| `'echo "To print basic information about a command use tldr <command>'` | `helpme`       | Tldr will teach you to use a command                       |                                             
| `sudo -H DIFFPROG=meld pacdiff`                                         | `pacdiff`      | compare local package database and remote package database |                                             
| `journalctl -p 3 -xb`                                                   | `jctl`         | View logs                                                  |                                             
| `expac --timefmt="%Y-%m-%d %T" "%l\t%n %v" | sort | tail -200 | nl`     | `rip`          | Check package repository                                   |  
                              
### Navigation                                                                                  
Instead of typing the whole `cd /path` command to move back a directory, you can just substitute with `.` the number   
of times you would love to back.  

                                                                        
| Command           | Aliases/Shortcut | 
|-------------------|------------------|  
| cd ..             | `..`             |                                                                               
| cd ../..          | `...`            |                                                                               
| cd ../../..       | `....`           |                                                                               
| cd ../../../..    | `.....`          |                                                                               
| cd ../../../../.. | `......`         |                                                                               
### Helpful Commands
This include system commands, or common commands but modified in a way to make life much more easier for a garuda user.
                                                                                           
| Command                               | Alias/Shortcut   | Purpose                                     |
|---------------------------------------|------------------|---------------------------------------------| 
| `expac -H M "%m\t%n" | sort -h | nl`  | `big`            | Sort Installed packages according to size   |                                                                                            
| `sudo rm /var/lib/pacman/db.lck`      | `fixpacman`      | Remove lock from database to enable install |                                                                                            
| `dir --color=auto`                    | `dir`            | List directories in color                   |                                                                                            
| `pacman -Q | grep -i "\-git" | wc -l` | `gitpk`          | List amount of gitpkgs                      |                                                                                            
| `ugrep --color=auto`                  | `grep`           | Appealing visuals of grep                   |                                                                                            
| `'ugrep -E --color=auto`              | `egrep`          | Appealing visuals of egrep                  |                                                                                            
| `ugrep -F --color=auto`               | `fgrep`          | Appealing visuals of ugrep                  |                                                                                            
| `sudo update-grub`                    | `grubup`         | Fix Grb                                     |                                                                                            
| `hwinfo --short`                      | `hw`             | Show hardware info                          |                                                                                            
| `ip -color`                           | `ip`             | Show your ip but in color                   |                                                                                            
| `ps auxf | sort -nr -k 4`             | `psmem`          | Show process sorted by cpu usage            |                                                                                            
| `ps auxf | sort -nr -k 4 | head -10`  | `psmem10`        | Cpu usage for first 10                      |                                                                                            
| `sudo pacman -Rdd`                    | `rmpkg`          | Remove packages                             |                                                                                            
| `tar -acf`                            | `tarnow`         | Create compressed archive file              |                                                                                            
| `tar -zxvf`                           | `untar`          | Unzip tar archive                           |                                                                                            
| `garuda-update`                       | `upd`            | Update garuda linux                         |                                                                                            
| `vdir --color=auto`                   | `vdir`           | List directories in color                   |                                                                                            
| `wget -c`                             | `wget`           | Continue downloading even when forgeting -c |     


## Easiest Way to Deal with mirrors                                                                                                                                                                                        
                                     
| Command                                                                               | Alias     | Function                                                  |
|---------------------------------------------------------------------------------------|-----------|-----------------------------------------------------------|
| `sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist`    | `mirror`  | Update your mirror list by choosing fastest recent mirror |                                     
| `sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist`   | `mirrora` | Update by selecting 20 most recent mirrors                |                                     
| `sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist` | `mirrord` | Update your mirror based on delay                         |                                     
| `sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist` | `mirrors` | 20 best mirrors based on their score                      | 
